class Modules {
  constructor(modules) {
    this.modules = modules || {};
  }

  check(name) {
    return this.modules.hasOwnProperty(name);
  }

  set(module) {
    this.modules[module.name] = module;
  }

  get(name) {
    return this.modules[name];
  }
}

module.exports = Modules; 
