class Module {
  constructor(name, path, needed, closed) {
    this.name = name;
    this.path = path;
    this.needed = needed || [];

    //Если true то для использования необходима авторизация
    this.closed = (closed === undefined) ? true : closed;
  }

  check(registry) {
    return registry.check(this.needed);
  }

  handle(registry, Respons) {
    return require(this.path)(registry, Respons);
  }
}

module.exports = Module;
