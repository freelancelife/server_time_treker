const { Pool } = require('pg');

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false
  }
});

function query(sql, params, callback) {
  if(callback) {
    return pool.query(sql, params, callback);
  }

  return new Promise((resolve, reject) => {
    return pool.query(sql, params, (err, res) => {
      if(err) {
        return reject(err);
      }

      return resolve(res);
    });
  })
}

module.exports = {
  query 
}
