class Respons {
  constructor(data, error, log) {
    this.error = error || 0;
    this.status = (this.error === 0) ? 'ok' : 'error';
    this.response = data || {};
    this.log = log;
  }

  json() {
    return JSON.stringify(this);
  }
}

module.exports = Respons;
