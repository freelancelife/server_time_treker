class Manager {
  constructor(modules, Respons, Registry, auth) {
    this.modules = modules || {};
    this.Respons = Respons; 
    this.Registry = Registry;
    this.auth = auth;
  }


  async handle(data) {
    if(!this.modules.check(data.cmd)) {
      return new this.Respons(null, 1); 
    }

    var module = this.modules.get(data.cmd);
    var auth = null;

    if(module.closed) {
      auth = await this.auth.authorization(data.sig);
    }

    var registry = new this.Registry(data, auth);

    if(!registry.checkDataArray(module.needed)) {
      console.log('REGISTRY: ', registry.data);
      return new this.Respons(null, 3);
    }

    return module.handle(registry, this.Respons);
  }
}

module.exports = Manager;
