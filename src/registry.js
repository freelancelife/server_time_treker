class Registry {
  constructor(req, auth) {
    this.cmd = req.cmd || null;
    this.data = req.data || {};
    this.sig = req.sig || null;
    this.isParser = !(typeof req.data === "object");
    this.auth = auth || {}; 
  }

  checkData(name) {
    if(this.isParser) {
      this.json();
    }

    return this.data.hasOwnProperty(name);
  }

  checkDataArray(names) {
    return names.every(name => this.checkData(name));
  }

  getData(name) {
    if(this.isParser) {
      this.json();
    }

    return this.data[name];
  }

  json() {
    if(this.isParser) {
      try{
        this.data = JSON.parse(this.data);
        this.isParser = false;
      } catch (err) {
        this.data = {};
      }
    }
  }
}

module.exports = Registry;
