const EventEmiter = require('events');

class Proxy extends EventEmiter {
  constructor(w, cached) {
    super();

    this.wraperPromise = w;

    this.didInvalidate = true;
    this.isLoading = false;
    this.isError = false;
    this.error = null;
    this.queur = [];

    this.cache = cached || true;
    this.cacheData = null;
    this.cacheDate = null;

    this.done();
    this._run();
  }

  done() {
    this.on('done', () => {

      this.queur.forEach((f) => {
        f(this.error, this.cacheData);
      });

      this.queur = [];
    })
  }

  _run() {
    this.on('run', (arg) => {
      console.log('autorization step1', this);
      this.isLoading = true;
      this.didInvalidate = false;
      console.log('autorization step2', this);

      this.wraperPromise(...arg)
      .then(data => {
        this.cacheData = data; 
        this.cacheDate = new Date();

        this.isLoading = false;
        this.DidInvalidate = false;
        this.isError = false;
        this.error = null;

        this.emit('done');
      })
      .catch(err => {
        console.log('proxy error', err);
        this.isLoading = false;
        this.isError = true;
        this.error = err;

        this.emit('done');
      });
    
    });
  }

  run() {
    if(this.didInvalidate) {
      return new Promise((resolve, reject) => {
        this.queur.push((err, data) => { 
          if(err) return reject(err); 

          return resolve(data);
        });
          
        this.emit('run', arguments);
      });
    }

    if(this.isLoading) {
      return new Promise((resolve, reject) => {
        this.queur.push((err, data) => { 
          if(err) return reject(err); 

          return resolve(data);
        });
      }); 
    }

    if(this.isError) {
      return Promise.reject(this.error);
    }

    return Promise.resolve(this.cacheData);
  }

  invalidate() {
    this.didInvalidate = true; 
    this.cacheData = null;
    this.cacheDate = null;
  }
}

module.exports = Proxy;
