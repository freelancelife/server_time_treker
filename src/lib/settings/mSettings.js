const { Model, Fields, Field } = require("../model");

class MSettings extends Model {
    constructor() {
        super();
        this.name = 'settings';

        this.fields = new Fields(
            new Field('key', 'text', null, 'PRIMARY KEY'),
            new Field('value', 'json', null),
            new Field('last_change', 'date', new Date())
        );
    }
}

module.exports = new MSettings();
