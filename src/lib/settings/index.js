const mSettings = require('./mSettings');
const { db, sql } = require("../model");

db.query(new sql.Create(mSettings))
.then(() => console.log('create table Settings'))
.catch(err => console.log('Error create table Settings: ', err));

function set(key, value) {
  const params = {
    key,
    value,
    last_change: new Date()
  };
  return db.query(new sql.InsertOrUpdate(mSettings, params));
}

function get(key, value) {
  return db.query(new sql.Select(mSettings, {where:{key}}))
  .then(table => {
    if(table.rows.length === 0) {
      return Promise.resolve(null);
    }

    return Promise.resolve(table.rows[0].value);
  })
}

function del(key) {
  return db.query(new sql.Delete(mSettings, {where:{key}}));
}

module.exports = {
  set,
  get,
  del
}
