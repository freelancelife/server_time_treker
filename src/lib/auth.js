const jwt = require('jsonwebtoken');

// @param userName string
// @param password string
// @param secret string
// @param callback function
// @return token string
function authentication(userName, password, callback) {

    if (this.userName !== userName ||  this.password !== password) {
        return callback( new Error('аутентификация провалена',
            'authentivation error')
        );
    }

    return jwt.sign({...this.data}, this.secret, callback);
}

// @param token string
// @param secret string
// @return calback
function authorization(token, callback) {
    if(callback) {
      return jwt.verify(token, this.secret, callback); 
    }

    return new Promise((resolve, reject) => {
      return jwt.verify(token, this.secret, (err, decode) => {
        if(err) {
          reject(err);
        }

        return resolve(decode);
      })
    })
}

function middleware(req, res, next) {
  console.log('authorization token: ', req.headers.token);
  jwt.verify(req.headers.token, this.secret, (err, decode) => {
    if(err) {
      return res
        .status(401)
        .send({error: err.message, message: 'not authorization!'})
    }
    return next();
  });
}

function createToken(data, callback) {
  return jwt.sign({...data}, this.secret, callback);
}

//@param conf { userName, password, secret }

module.exports = function(conf){
  return {
    authentication: authentication.bind(conf),
    authorization: authorization.bind(conf),
    middleware: middleware.bind(conf),
    createToken: createToken.bind(conf)
  }
}
