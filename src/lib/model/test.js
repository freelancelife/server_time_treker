const db = require('./db');
const Model = require('./model.js');
const Fields = require('./fields.js');
const Field = require('./field.js');
const { Create, Select, Update, Insert } = require('./sql');

class mTest extends Model {
  constructor() {
    super();

    this.name = 'test';
    this.fields = new Fields(
      new Field('name', 'text', null),
      new Field('pool', 'text', null),
      new Field('last_change', 'date', Date),
    ); 
  }
}

const test = new mTest();

//db.query(new Create(test) )
//  .then( res => console.log(res) )
//  .catch( err => console.log(err) );
//
//const iTest = new Insert(test, {name: 'toxa', pool: 'xer'});
//db.query(iTest)
//  .then( res => console.log(res) )
//  .catch( err => console.log(err) );

//db.query( new Select(test, {where:{name: ['lexa']}}) )
//  .then( res => console.log(res) )
//  .catch( err => console.log(err) );

db.query( new Update(test, {filter: ['lexa'], filterName: 'name', value: {name: 'shit'}}) )
  .then( res => console.log(res) )
  .catch( err => console.log(err) );

//db.query( new Select(test, {where: {name: ['toxa']}}) )
//  .then( res => console.log(res) )
//  .catch( err => console.log(err) );
