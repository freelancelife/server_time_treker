class Field {
  constructor() {
    var [
      name = 'name',
      type = 'integer',
      value = null,
      ...params
    ] = Array.from(arguments);

    this.primaryKey = params.includes('PRIMARY KEY');
    this.name = name;
    this.type = type;
    this.params = params;
    this.value = value;
    this.templateType = 'input&string';

  }

  valueOf() {
    return [this.name, this.type, ...this.params];
  }

  setTemplateType(value) {
    this.templateType = value;

    return this;
  }

  format(value) {
    if (this.type === 'timestamp') {
      return new Date(value);
    }

    return value;
  }

}

module.exports = Field;
