const needle = require('needle');
/*
 * @param {object Sql} sql
 * @param {function} callback необязательный
 * @return {Promise} если не передали callbdck
 */
function query(sql, callback) {
  var string = sql.toString();
  var params = sql.params();

  //console.log('sql: ', string, 'params: ', params);

  if(callback) {
    return pool.query(string, params, callback);
  }


  return new Promise( function (resolve, reject) {
    return pool.query(string, params, (err, res) => {
      if(err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
}

function sqlReq(sql, params) {
  const host = 'http://localhost:5000';
  const url = host + '?cmd=sql2'
  + '&data=' JSON.stringify({sql, params}) 
  + '&sig=11'

  needle('get', url)
  .then(console.log)
  .catch(console.log);
}

module.exports = {
  query
}

