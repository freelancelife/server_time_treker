const { Pool } = require('pg');

const DATABASE_URL = process.env.DATABASE_URL 

const pool = new Pool({
  connectionString: DATABASE_URL,
  ssl: {
    rejectUnauthorized: false
  }
});

/*
 * @param {object Sql} sql
 * @param {function} callback необязательный
 * @return {Promise} если не передали callbdck
 */
function query(sql, callback) {
  var string = sql.toString();
  var params = sql.params();

  console.log('sql: ', string, 'params: ', params);

  if(callback) {
    return pool.query(string, params, callback);
  }

  return new Promise( function (resolve, reject) {
    return pool.query(string, params, (err, res) => {
      if(err) {
        return reject(err);
      }

      return resolve(res);
    });
  });
}

module.exports = {
  query
}

