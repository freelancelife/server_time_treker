const http = require('http');
const querystring = require('querystring');

module.exports = function(sql, params, callback) {

  const postData = querystring.stringify({
    sql, params: JSON.stringify(params)
  }); 
  const options = {
    hostname: process.env.API_BD_HOST,
    port: 80,
    //hostname: '127.0.0.1',
    //port: 4000,
    path: '/?cmd=sql&sig='+process.env.API_BD_TOKEN,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(postData)
    }
  };

  const req = http.request(options, (res) => {
    var data = "";

    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      data += chunk;
    });
    res.on('end', () => {
      try {
        data = JSON.parse(data);
        console.log('DB GET API: ', data);
        if(data.status.toUpperCase() === 'OK') {
          return callback(null, data);
        }
      } catch(err) {
        console.log('API.MOTOSHAR ERROR: json parse: ', data);
        return callback(err, data);
      }

      console.log('ERROR: ', data);

      callback(Error('request status no ok'), data);
    });
  });

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`);
    callback(e);
  });

  // write data to request body
  req.write(postData);
  req.end();
}

