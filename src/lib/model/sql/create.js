const Sql = require('./sql.js');

class Create extends Sql {
  constructor(model) {
    super(model);
  }

  toString() {
    return 'CREATE TABLE if not exists ' + this.model.name + '(' + 
      this.model.fields.map(el => el.valueOf().join(' ')) + ');';
  }
}

module.exports = Create;
