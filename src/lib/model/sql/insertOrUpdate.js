const Insert = require('./insert.js');

class InsertOrUpdate extends Insert {
  constructor(model, set) {
    super(model, set);
    this.header = 'INSERT INTO';
  }

  conflictValues() {
    return this.model.fields 
      .filter(field => !field.primaryKey )
      .map(field => {
        return `${field.name} = EXCLUDED.${field.name}`;
      })
  }

  toString() {
    let fields = this.model.fields
      .filter( el => el.value !== 'undefined' );
    let fieldsNames = fields.map( el => el.name ); 
    let fieldsValues = this.values().join(',');
    let conflictValues = this.conflictValues().join(',');

    return `${this.header} ${this.model.name}(${fieldsNames}) VALUES${fieldsValues} ON CONFLICT (${this.model.primaryKey()}) DO UPDATE SET ${conflictValues}`;
  }
}

module.exports = InsertOrUpdate;
