const Sql = require('./sql.js');

class Drop extends Sql {
  constructor(model) {
    super(model);
  }

  toString() {
    return 'DROP TABLE ' + this.model.name + ';';
  }

}

module.exports = Drop;
