const Sql = require('./sql.js');

class CustomSql extends Sql {
  constructor(string, params) {
    super({});
    this._sql = string;
    this._params = params || [];
  }

  params() {
    return this._params;
  }

  toString() {
    return this._sql;
  }
}

module.exports = CustomSql;
