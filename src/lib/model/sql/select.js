const Sql = require('./sql.js');
const where = require('./lib/where');

class Select extends Sql {

  constructor(model, obj = {}) {
    super(model);
      this.filter = {
        select: '*', 
        from: model.name,
        ...obj
      }
    }
  
  /*
   * @param {Object} 
   * @return {Array}
   */
  where(obj) {
    return where(obj);
  }

  toString() {
    let arr = [];

    forObj(this.filter, (key, value) => {
      switch(key) {
        case 'where':
          return arr.push(this.where(value));

        default:
          return arr.push(key + ' ' + value);
      }   
    });
    
    return arr.join(' ') + ';';
  }
}

function forObj(obj, callback) {
  for(var key in obj) {
    callback(key, obj[key]);
  }
}

function join(obj, delemite) {
  let arr = [];

  for(var key in obj) {
    if(obj[key]) {
      arr.push(key + delemite + obj[key]);
    }  
  }

  return arr;
}

module.exports = Select;
