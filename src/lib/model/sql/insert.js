const Sql = require('./sql.js');

class Insert extends Sql {
  constructor(model, set) {
    super(model);
    this.multi = false;

    this.set = (set instanceof Array) ?
      (this.multi = true, this.model.paramsMulti(set)) : this.model.params(set);


    this._params = (this.multi) ?
      [].concat.apply([], this.set) : this.set;
  }

  values() {
    var i = 1;

    if(this.multi) {
      return this.set.map( set => 
        '(' + set.map( () => '$' + (i++) ) + ')'
      );
    }

    return ['(' + this.set.map( () => '$' + (i++) ) + ')'];
  }

  toString() {
    let fields = this.model.fields
      .filter( el => el.value !== 'undefined' );
    let fieldsNames = fields.map( el => el.name );
    let fieldsValues = this.values().join(',');

    return 'INSERT INTO '+this.model.name+'('+ fieldsNames +') VALUES'+ fieldsValues +';';
  }
}

module.exports = Insert;
