const Sql = require('./sql.js');
const where = require('./lib/where');

class Update extends Sql {
  constructor(model, {where, set}) {
    super(model);

    this.set = set;
    this.filter = where || {}

    this.sql();
  }

  where(obj) {
    return where(obj);
  }

  sql() {
    let arr = this.model.fields
      .filter(el => this.set.hasOwnProperty(el.name));
    let fieldsName = arr.map((el,id) => el.name + '=$' + (id + 1));

    this._params = arr.map(el => this.set[el.name]);

    this._sql = 'UPDATE ' + this.model.name + ' SET ' + fieldsName + ' ' + this.where(this.filter) +';';
  }
}


module.exports = Update;
