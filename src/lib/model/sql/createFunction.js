const Sql = require('./sql.js');

class CreateFunction extends Sql {
  constructor(model) {
    super(model);
  }

  getParams() {
    return this.model.fields
      .map(el => el.name + this.model.name + ' ' + el.type )
      .join(',')
      .replace('bigserial', 'bigint');
  }

  getParamsUpdate() {
    return this.model.fields
      .filter(el => !el.primaryKey)
      .map(el => el.name  + '=' + el.name + this.model.name)
      .join(',');
  }

  getPrimaryKeyUpdate() {
    let primaryKey = this.model.fields.find(el => el.primaryKey)

    return `${primaryKey.name} = ${primaryKey.name}${this.model.name}`;
  }

  getParamsInsert() {
    return this.model.fields
      .map(el => el.name)
      .join(',');
  }

  getParamsInsertValue() {
    return this.model.fields
      .map(el => el.name + this.model.name)
      .join(',');
  }

  toString() {
    return `CREATE FUNCTION ${this.model.name}(${this.getParams()}) RETURNS VOID AS $$ BEGIN LOOP UPDATE ${this.model.name} SET ${this.getParamsUpdate()} WHERE ${this.getPrimaryKeyUpdate()}; IF found THEN RETURN; END IF; BEGIN INSERT INTO ${this.model.name}(${this.getParamsInsert()}) VALUES (${this.getParamsInsertValue()}); RETURN; EXCEPTION WHEN unique_violation THEN END; END LOOP; END; $$ LANGUAGE plpgsql;`;
  }
}

module.exports = CreateFunction;
