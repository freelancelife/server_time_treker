const Create = require('./create');
const Select = require('./select');
const Insert = require('./insert');
const InsertOrUpdate = require('./insertOrUpdate');
const InsertOrUpdate92 = require('./insertOrUpdate_9_2');
const Update = require('./update');
const Delete = require('./delete');
const Drop = require('./drop');
const CustomSql = require('./customSql');
const CreateFunction = require('./createFunction');

module.exports = {
  Create,
  Select,
  Insert,
  InsertOrUpdate,
  InsertOrUpdate92,
  Update,
  Delete,
  Drop,
  CustomSql,
  CreateFunction
}
