module.exports = where;

/*
 * @param {Object} 
 * @return {Array}
 */
function where(obj) {
  let arr = [];

  forObj(obj, (key, value) => {
    if(value === null) {
      return arr.push(`${key} is NULL`);
    }

    if(value instanceof Array) {
      if(value.length) {
        return arr.push(`${key} in (${value.map(el => `'${el}'`)})`);
      }
      return; 
    }

    if(value instanceof Object && value.hasOwnProperty('from') && value.hasOwnProperty('to')) {
      return arr.push(`(${key} between '${value.from}' and '${value.to}')`);
    }

    return arr.push(`${key} = '${value}'`);
  })

  if(arr.length === 0) {
    return '';
  }

  return 'where ' + arr.join(' AND ');
}

function forObj(obj, callback) {
  for(var key in obj) {
    callback(key, obj[key]);
  }
}
