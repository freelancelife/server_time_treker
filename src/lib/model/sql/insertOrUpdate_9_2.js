const Insert = require('./insert.js');

class InsertOrUpdate92 extends Insert {
  constructor(model, set) {
    super(model, set);
  }

  toString() {
    let fieldsValues = this.values().join(',');

    return `SELECT ${this.model.name}${fieldsValues};`;
  }
}

module.exports = InsertOrUpdate92;
