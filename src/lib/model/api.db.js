const get = require('./api.get');

function query(sql, callback) {
  var string = sql.toString();
  var params = sql.params();

  //console.log('sql: ', string, 'params: ', params);

  if(string[string.length - 1] !== ";") {
    string += ";"; 
  }

  if(callback) {
    return get(string, params, callback);
  }

  return new Promise( function (resolve, reject) {
    console.log('api.db: ', params);
    return get(string, params, (err, res) => {
      if(err) {
        return reject(err);
      }

      return resolve({rows: res.response});
    });
  });
}

module.exports = {
  query
}

