//Инициализируем глобальные переменные
require('../../config.init.js')();
require('../../model/init')();

const authentication = require('./authentication');
const authorization = require('./authorization');
const createAnAccount = require('./createAnAccount');

createAnAccount({email: 'test@email', user_name: 'test', password: 'test', type: 'user'})
.then(console.log)
.catch(console.log)

authentication({user_name: 'test', password: 'test'})
.then(authorization)
.then(console.log)
.catch(console.log)
