const mUsers = require('./mUser');
const {db, sql} = require('../model'); 
const jwt = require('jsonwebtoken');

// @param userName string
// @param password string
// @param secret string
// @param callback function
// @return token string
function authentication({user_name, password}) {
  let searchUser = new sql.Select(mUsers, {
    select: 'id, type, user_name',
    where: {user_name, password}
  });
  
  return db.query(searchUser)
  .then(table => table.rows)
  .then(users => {
    if(users.length !== 1) {
      return Promise.reject(new Error('Аутентификация провалена'));
    }

    return jwt.sign({...users[0]}, process.env.AUTH_SECREAT);
  })

}

module.exports = authentication;
