const { Model, Fields, Field } = require("../model");

class User extends Model {
    constructor() {
        super();
        this.name = 'users';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY'),
            new Field('date_create', 'timestamp', new Date()),
            new Field('type', 'text', null),
            new Field('password', 'text', null),
            new Field('user_name', 'text', null),
            new Field('email', 'text', null),
        );
    }
}

module.exports = new User();


