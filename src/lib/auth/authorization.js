const jwt = require('jsonwebtoken');

// @param token string
// @param secret string
// @return calback
function authorization(token, callback) {
    if(callback) {
      return jwt.verify(token, process.env.AUTH_SECREAT, callback); 
    }

    return new Promise((resolve, reject) => {
      return jwt.verify(token, process.env.AUTH_SECREAT, (err, decode) => {
        if(err) {
          reject(err);
        }

        return resolve(decode);
      })
    })
}

module.exports = authorization;
