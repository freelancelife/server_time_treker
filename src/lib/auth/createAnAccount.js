const {db, sql} = require('../model'); 
const mUsers = require('./mUser');

module.exports = function(set) {
  let searchUser = new sql.Select(mUsers, {where: {email: set.email}});
  let createUser = new sql.Insert(mUsers, set);

  return db.query(searchUser)
  .then(table => table.rows)
  .then(users => {
    if(users.length !== 0) {
      throw new Error('Такой пользователь уже существует');
      return null;
    }

    return db.query(createUser);
  })
}
