const {db, sql} = require('../lib/model');
const model = require('../model');

module.exports = (registry, Respons) => {
  const modelName = registry.getData('model');
  if(!model.includes(modelName)) {
    return Promise.reject(new Respons(null, 40));
  }

  const auth = registry.auth;
  const set = { ...registry.getData('set'), user_id: auth.id};
  const insert = new sql.Insert(model.get(modelName), set);

  return db.query(insert)
  .then(table => new Respons(table));
}
