const authentication = require('../lib/auth/authentication');

module.exports = (registry, Respons) => {
  return authentication(registry.getData('where'))
  .then(token => new Respons({token}))
}
