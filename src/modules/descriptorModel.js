const models = require('../model');

module.exports = (registry, Respons) => {
  const modelName = registry.getData('model');
  if(!models.includes(modelName)) {
    return Promise.reject(new Respons(null, 40));
  }
  
  const model = models.get(modelName);

  const keys = model.fields.map(field => field.name);
  const fields = {};
  model.fields.forEach(field => fields[field.name] = field.templateType);

  return new Respons({keys, fields});
}
