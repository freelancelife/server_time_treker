const pg = require('../pg');

module.exports = (registry, Respons) => {
  const auth = registry.auth;

  let sql = 'select * from ' + registry.getData('model') + ';';

  if(registry.checkData('select')) {
    sql = sql.replace('*', registry.getData('select'));
  }

  var whereParams = {user_id: auth.id};
  if(registry.checkData('where')) {
    whereParams = {...registry.getData('where'), user_id: auth.id};
  }
  sql = sql.replace(';', where(whereParams)) + ';';

  return pg.query(sql)
  .then(table => new Respons(table.rows));
}

function where(obj) {
  let where = [];

  for(let key in obj) {
    if(obj[key] === null) {
      where.push(key + ' is NULL');
      continue;
    }

    if(typeof obj[key] === 'string') {

      //if(obj[key].search(/^[0-9]+-[0-9]+-[0-9]+T[0-9]+:[0-9]+:[0-9]+.[0-9]+Z$/) !== -1) {
      //  where.push(`${key} = '${obj[key].split('T')[0]}'`);
      //  continue;
      //}
      //
      if(obj[key].includes('LIKE')) {
        where.push(`${key} LIKE '%${obj[key].split("-")[1]}%'`);
        continue;
      }

      where.push(`${key} = '${obj[key]}'`);
      continue;
    }

    if(obj[key] instanceof Array) {
      if(key.includes('date')) {
        let d = obj[key].map(d => 
          `(${key} between '${d} 00:00:00' and '${d} 23:59:59')`
        ).join(' OR ')
        where.push(`(${d})`);
        continue;
      }

      where.push(`${key} in (${obj[key].map(d => `'${d}'`)})`);
      continue;
    }

    if(typeof obj[key] === 'object') {
      if(obj[key].hasOwnProperty('from') && obj[key].hasOwnProperty('to')) {
        where.push(`${key}>='${obj[key].from}' and ${key}<='${obj[key].to}'`);
        continue;
      }
    }

    where.push(key + "=" + obj[key]);
  }

  return ' where ' + where.join(' AND ');
}
