const pg = require('../pg');

module.exports = function(registry, Respons) {
  var sql = registry.getData('sql');
  var params = registry.getData('params');

  console.log('PARAMS: ', params);

  if(params) {
    params = params.map(el => (el === '') ? null : el);
  }

  return new Promise((resolve, reject) => {
    pg.query(sql, params || [], (err, table) => {
      if(err) {
        return reject(new Respons([], 10, {err, sql}));
      }

      return resolve(new Respons(table.rows, null, {sql, params}));

    });
  })
}
