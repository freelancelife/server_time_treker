const {db, sql} = require('../lib/model'); 
const model = require('../model');

module.exports = (registry, Respons) => {
  const modelName = registry.getData('model');
  if(!model.includes(modelName)) {
    return Promise.reject(new Respons(null, 40));
  }

  const auth = registry.auth;
  const set = registry.getData('set');
  const where = {...registry.getData('where'), user_id: auth.id};

  const update = new sql.Update(model.get(modelName), {set, where});

  console.log(update.toString());
  console.log(update.params());

  return db.query(update)
  .then(table => new Respons(table.rows));
}
