const excel = require('excel4node');
const {db, sql} = require('../lib/model');
const model = require('../model');

module.exports = (registry, Respons) => {
  const auth = registry.auth;
  const where = {...registry.getData('where'), user_id: auth.id};

  var bookName = 'Sheet 1';
  var fileName = 'Excel';
  if(where.hasOwnProperty('date_create')
  && where.date_create.hasOwnProperty('to')
  ) {
    let date = new Date(where.date_create.to); 
    bookName = date.toLocaleString('en', {
      year: 'numeric', month: 'long'
    })
    fileName = date.toLocaleString('en', {
      year: 'numeric', month: 'long'
    }).replace(' ', '')

    console.log(bookName, fileName);
  }

  // Create a new instance of a Workbook class
  var workbook = new excel.Workbook();

  // Create a style center
  var styleCenter = workbook.createStyle({
    alignment: {
        horizontal: 'center'
    }
  });

  var borderDefoul = {
          style: "thin", //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
          color: '000000' // HTML style hex value
      }

  var borderConf = { // §18.8.4 border (Border)
      top: borderDefoul,
      bottom: borderDefoul 
  }

  var styleBorder = workbook.createStyle({
    border: borderConf 
  })

  var borderB = {
          style: "medium", //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
          color: 'cccccc' // HTML style hex value
      }
  var styleB = workbook.createStyle({
    border: {
      top: borderB, 
      bottom: borderB,
      left: borderB,
      right: borderB,
      outline: true
    } 
  })

  var styleKL = workbook.createStyle({
    border: {
      top: borderDefoul, 
      bottom: borderDefoul,
      left: borderDefoul,
      right: borderDefoul,
      outline: true
    }, 
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: 'daeef3', // HTML style hex value. defaults to black
      fgColor: 'daeef3' // HTML style hex value. defaults to black.
    }
  })

  var styleNO = workbook.createStyle({
    border: {
      top: borderDefoul, 
      bottom: borderDefoul,
      left: borderDefoul,
      right: borderDefoul,
      outline: true
    }, 
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: 'fde9d9', // HTML style hex value. defaults to black
      fgColor: 'fde9d9' // HTML style hex value. defaults to black.
    }
  })

  var styleD = workbook.createStyle({
    border: borderConf,
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: 'dce6f1', // HTML style hex value. defaults to black
      fgColor: 'dce6f1' // HTML style hex value. defaults to black.
    },
  })
  var styleE = workbook.createStyle({
    border: borderConf,
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: 'ebf1de', // HTML style hex value. defaults to black
      fgColor: 'ebf1de' // HTML style hex value. defaults to black.
    },
  })
  var styleF = workbook.createStyle({
    border: borderConf,
    fill: {
      type: 'pattern',
      patternType: 'solid',
      bgColor: 'daeef3', // HTML style hex value. defaults to black
      fgColor: 'daeef3' // HTML style hex value. defaults to black.
    },
  })
  var styleFontBold = workbook.createStyle({
    font: {
      bold: true
    }
  })

  // Add Worksheets to the workbook
  var worksheet = workbook.addWorksheet(bookName);
  var ws = worksheet;

  ws.addConditionalFormattingRule('A1:A30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleBorder, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('B1:B30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleB, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('D1:D30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleD, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('E1:E30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleE, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('F1:F30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleF, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('G1:G30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleBorder, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('H1:H30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleBorder, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('I1:I30', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleBorder, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('K1:L7', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleKL, // a style object containing styles to apply
  });
  ws.addConditionalFormattingRule('N1:O6', {
    // apply ws formatting ref 'A1:A10'
    type: 'expression', // the conditional formatting type
    priority: 1, // rule priority order (required)
    formula: '1', // formula that returns nonzero or 0
    style: styleNO, // a style object containing styles to apply
  });

  //worksheet.cell(1, 1).string('Date');
  //worksheet.cell(1, 2).string('Client');
  //worksheet.cell(1, 3).string('Description');
  //worksheet.cell(1, 4).string('Time spent');
  //worksheet.cell(1, 5).string('Time start');
  //worksheet.cell(1, 6).string('Time stop');
  //worksheet.cell(1, 7).string('Time all');

  //worksheet.column(1).style = styleBorder;

  worksheet.cell(1, 1).string('Дата')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 2).string('Клиент')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 3).string('Описание задачи')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 4).string('Труд.')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 5).string('Транс.')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 6).string('Фикс.')
    .style(styleFontBold)
    .style(styleCenter);
  worksheet.cell(1, 7).string('Начало').style(styleFontBold);
  worksheet.cell(1, 8).string('Окончание').style(styleFontBold);
  worksheet.cell(1, 9).string('Длительность')
  .style(styleFontBold)
  .style({alignment:{horizontal: 'fill'}});

  worksheet.column(2).setWidth(20);
  worksheet.column(3).setWidth(50);
  worksheet.column(4).setWidth(6);
  worksheet.column(5).setWidth(7);
  worksheet.column(6).setWidth(7);
  worksheet.column(7).setWidth(6);
  worksheet.column(8).setWidth(6);
  worksheet.column(9).setWidth(6);

  worksheet.column(10).setWidth(6);

  // BEGITN Рачёт 
  worksheet.cell(1,11,1,12, true).string('Расчёт')
    .style(styleCenter)
    .style(styleFontBold);
  worksheet.cell(2,11).string('Часов');
  worksheet.cell(3,11).string('Транспортные');
  worksheet.cell(4,11).string('Фиксированные');
  worksheet.cell(5,11).string('На карту');
  worksheet.cell(6,11).string('Авансы');
  worksheet.cell(7,11).string('К выдаче');

  worksheet.column(11).setWidth(15);

  worksheet.cell(2,12).number(0);
  worksheet.cell(3,12).number(0);
  worksheet.cell(4,12).number(0);
  worksheet.cell(5,12).number(0);
  worksheet.cell(6,12).number(0);
  worksheet.cell(7,12).number(0);
  // BEGITN
  
  worksheet.column(13).setWidth(6);

  // BEGITN Аваны 
  worksheet.cell(1,14,1,15, true).string('Авансы')
    .style(styleFontBold)
    .style(styleCenter);
  // BEGITN


  return getData(where) 
  //.then(tables => tables.map(table => table.rows))
  .then(([tasks, timers, clients, price]) => {
    var hourAll = 0;
    var priceAll = 0;
    var fixedPriceAll = 0;

    tasks.forEach((task, id) => {
      const client = clients.find(client => client.id === task.client_id);

      let timersId     = timers.filter(el => el.task_id === task.id);
      let timeBegin    = 0;
      let timeEnd      = 0; 
      let timeAll      = Time(getTime(timersId));
      let hour         = Number(timeFormatH(timeAll));
      let description  = (task.description) ?
          task.title+' - '+task.description : task.title;

      //Подумать как реализовать.
      if (false) {
        worksheet.cell(id + 2, 7)
          .string(timeFormat(timeBegin))
          .style(styleBorder);
        worksheet.cell(id + 2, 8)
          .string(timeFormat(timeEnd))
          .style(styleBorder);
      }

      // Расчёт стоимости задачи
      hourAll += hour;
      
      if (price.task_id.hasOwnProperty(task.id)) {
        if (price.task_id[task.id].fixed) {
          fixedPriceAll += Number(price.task_id[task.id].price);

          worksheet.cell(id + 2, 6)
            .number(Number(price.task_id[task.id].price))
            .style(styleBorder);
        } else {
          priceAll += getPrice(price.task_id[task.id], hour);
        }
      } else if (price.client_id.hasOwnProperty(task.client_id)) {
        priceAll += getPrice(price.client_id[client_id.id], hour);
      } else if (price.category_id.hasOwnProperty(task.category_id)) {
        priceAll += getPrice(price.category_id[task.category_id], hour);
      }
      // Коне рачёта
      //
      if (client) {
        worksheet.cell(id + 2, 2)
          .string(client.title);
      }

      worksheet.cell(id + 2, 1)
        .date(new Date(task.date_create))
        .style({numberFormat: 'dd.mm.yyyy'});
      worksheet.cell(id + 2, 3)
        .string(description);
      worksheet.cell(id + 2, 4)
        .number(hour)
        .style(styleBorder);
      worksheet.cell(id + 2, 9)
        .string(timeFormat(timeAll))
        .style(styleBorder);
    })

    worksheet.cell(2,12).number(hourAll);
    worksheet.cell(4,12).number(fixedPriceAll);
    worksheet.cell(7,12).number(priceAll + fixedPriceAll);


    return {};
  })
  .then(() => (res) => workbook.write(fileName + '.xlsx', res));
}

function mergeTimersToTasks([tasks, timers]) {

}

async function getData(whereTasks) {
  const selectTasks = new sql.Select(model.get('task'), {where: whereTasks});
  const tasks = await db.query(selectTasks);
  const tasks_id = tasks.rows.map(el => el.id);

  const timers_filter = {where: {task_id: tasks_id}};
  const selectTimers = new sql.Select(model.get('timer'), timers_filter)
  const timers = await db.query(selectTimers);

  const client_filter = {where: {
    id: unique(tasks.rows.map(row => row.client_id).filter(row => !!row))
  }};
  const selectClient = new sql.Select(model.get('client'), client_filter)
  const clients = await db.query(selectClient);

  const price = await getPrices(tasks.rows);

  return [tasks.rows, timers.rows, clients.rows, price];
}

function getPrices(tasks) {
  const filterTask = {
    where: {
      task_id: tasks.map(task => task.id)
    }
  };
  const filterClient = {
    where: {
      client_id: unique(tasks
        .filter(task => !!task.client_id)
        .map(task => task.client_id))
    }
  };
  const filterCategory = {
    where: {
      category_id: unique(tasks
        .filter(task => !!task.category_id)
        .map(task => task.category_id))
    }
  };

  const selectPriceTask     = new sql.Select(
    model.get('price'), filterTask);
  const selectPriceClient   = new sql.Select(
    model.get('price'), filterClient);
  const selectPriceCategory = new sql.Select(
    model.get('price'), filterCategory);

  return Promise.all([
    db.query(selectPriceTask)
    .then(res => res.rows)
    .then(res => res.reduce((o, price) =>
      (o[price.task_id] = price, o), {})
    ),
    db.query(selectPriceClient)
    .then(res => res.rows)
    .then(res => res.reduce((o, price) =>
      (o[price.client_id] = price, o), {})
    ),
    db.query(selectPriceCategory)
    .then(res => res.rows)
    .then(res => res.reduce((o, price) =>
      (o[price.category_id] = price, o), {})
    ),
  ])
  .then(response => ({
    task_id: response[0],
    client_id: response[1],
    category_id: response[2],
  }))
  .catch(err => (console.log('CREATE ECXEL:', err), {
    task_id: {},
    client_id: {},
    category_id: {}
  }))
}

function unique(rows) {
  var filter = {};
  return rows.filter(row => {
    if (filter.hasOwnProperty(row)) {
      return false;
    }

    filter[row] = true;
    return true;
  });
}
 
function getPrice({fixed, price}, hours) {
  if (fixed) {
    return 0;
  }

  return (Number(hours) * Number(price));
}

function Time(time) {
  return { 
    h: Math.trunc(time / 3600000),
    m: Math.trunc((time / 60000) % 60),
    s: Math.trunc((time / 1000) % 60)
  }
}

function DateToTime(date) {
  if (typeof date === 'string') date = new Date(date);

  return { 
    h: date.getHours(),
    m: date.getMinutes(),
    s: date.getSeconds() 
  }
}

function timeFormat({h,m,s}) {

  h = h > 9 ? h : '0'+h;
  m = m > 9 ? m : '0'+m;
  //s = s > 9 ? s : '0'+s;

  return `${h}:${m}`;
  //return `${h}:${m}:${s}`;
}

function timeFormatH({h, m}) {
  return (h + (m / 60)).toFixed(2)
}

function getTime(timers) {
    return timers.reduce((sum, timer) => {
      const end = (typeof timer.timer_end_date === 'string') ?
        new Date(timer.timer_end_date) : timer.timer_end_date;
      const begin = (typeof timer.timer_begin_date === 'string') ?
        new Date(timer.timer_begin_date) : timer.timer_begin_date;

      return sum + (end - begin)
    }, 0);
  }
