/**
 * registry = {
 *  model: String,
 *  id: Number 
 * }
 **/
const pg = require('../pg');

module.exports = (registry, Respons) => {
  const auth = registry.auth;

  let sql = 'select * from '
    + registry.getData('model')
    + ' where id=$1 AND user_id=$2;';

  return pg.query(sql, [registry.getData('id'), auth.id])
  .then(table => new Respons(table.rows));
}
