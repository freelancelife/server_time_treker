const authentication = require('../lib/auth/authentication');
const createAnAccount = require('../lib/auth/createAnAccount');

module.exports = (registry, Respons) => {
  const set = registry.getData('set');

  return createAnAccount(set)
  .then(() => authentication(set))
  .then(token => new Respons({token}));
}
