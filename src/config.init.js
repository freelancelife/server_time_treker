const manifest = require('./config.manifest.json');

var config = {};
try {
  config = require(manifest.config);
} catch (err) {
  console.log('ERROR: ' + manifest.error[1] +  manifest.config);
}

module.exports = function init() {

for (let key in manifest.env) {

  switch(manifest.env[key]) {
    case 'require': {
      let error = setEnv(key);

      if( error !== 0) {
        console.log('ERROR: ', manifest.error[error] + ': ' + key); 
        process.exit(-1);
      }
      break;
    }

    default: {
      setEnv(key);
    }
  }
}

}

function setEnv(key) {
  if(!process.env.hasOwnProperty(key)) {

    if(config.hasOwnProperty(key)) {
      process.env[key] = config[key];
      return 0;
    } else {
      return 2 // Код ошибки смотри в manifest.json
    }
  }

  return 0;
}
