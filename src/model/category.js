const { Model, Fields, Field } = require("../lib/model");

class Category extends Model {
    constructor() {
        super();
        this.name = 'category';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY')
              .setTemplateType('delete'),
            new Field('parent', 'bigint', null)
              .setTemplateType('input&model&category'),
            new Field('user_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('delete'),
            new Field('title', 'text', null),
            new Field('descriptor', 'text', null)
              .setTemplateType('input&text'),
        );
    }
}

module.exports = new Category();
