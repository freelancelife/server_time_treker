const { Model, Fields, Field } = require("../lib/model");

class Price extends Model {
    constructor() {
        super();
        this.name = 'price';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY')
              .setTemplateType('delete'),

            new Field('user_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('delete'),

            new Field('task_id', 'bigint', null)
              .setTemplateType('input&model&task'),

            new Field('client_id', 'bigint', null)
              .setTemplateType('input&model&client'),
          
            new Field('category_id', 'bigint', null)
              .setTemplateType('input&model&category'),

            new Field('price', 'bigint', 0, 'NOT NULL')
              .setTemplateType('input&number'),

            new Field('fixed', 'boolean', false, 'NOT NULL')
              .setTemplateType('input&boolean'),

            new Field('date_create', 'timestamp', new Date())
              .setTemplateType('delete'),

            new Field('title', 'text', null),

            new Field('description', 'text', null)
              .setTemplateType('input-text'),
        );
    }
}

module.exports = new Price();
