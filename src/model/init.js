const {db, sql} = require('../lib/model'); 
const model = require('./index');

module.exports = () =>
model.listModelName.forEach(modelName => {
  return db.query(new sql.Create(model.get(modelName)))
  .then(() => console.log('CREATE TABLE:', modelName))
  .catch(console.log);
});
