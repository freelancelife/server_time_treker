const { Model, Fields, Field } = require("../lib/model");

class Task extends Model {
    constructor() {
        super();
        this.name = 'task';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY')
              .setTemplateType('delete'),
            new Field('user_id', 'bigint', null)
              .setTemplateType('delete'),

            new Field('title', 'text', null),
            new Field('description', 'text', null)
              .setTemplateType('input&text'),

            new Field('date_create', 'timestamp', new Date())
              .setTemplateType('input&date'),

            new Field('parent', 'bigint', null)
              .setTemplateType('input&model&task'),

            new Field('category_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('input&model&category'),
            new Field('client_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('input&model&client'),
        );
    }
}

module.exports = new Task();
