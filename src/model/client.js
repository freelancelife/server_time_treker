const { Model, Fields, Field } = require("../lib/model");

class Client extends Model {
    constructor() {
        super();
        this.name = 'client';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY')
              .setTemplateType('delete'),
            new Field('user_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('delete'),
            new Field('title', 'text', null),
            new Field('description', 'text', null)
              .setTemplateType('input&text'),
            new Field('client_name', 'text', null),
            new Field('phone', 'text', null),
            new Field('category_id', 'bigint', null, 'NOT NULL')
              .setTemplateType('input&model&category'),
        );
    }
}

module.exports = new Client();
