const listModelName = [
  'client',
  'task',
  'timer',
  'user',
  'category',
  'price'
]

module.exports = {
  get: (model) => {
    if(listModelName.includes(model)) {
      return require('./' + model); 
    }
    return undefined;
  },

  includes: (model) => {
    return listModelName.includes(model);
  },

  listModelName,
}
