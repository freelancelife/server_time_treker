const { Model, Fields, Field } = require("../lib/model");

class Timers extends Model {
    constructor() {
        super();
        this.name = 'timer';

        this.fields = new Fields(
            new Field('id', 'bigserial', 'undefined', 'PRIMARY KEY'),
            new Field('task_id', 'bigserial', null),
            new Field('user_id', 'bigserial', null),
            new Field('timer_begin_date', 'timestamp', new Date()),
            new Field('timer_end_date', 'timestamp', null),
        );
    }
}

module.exports = new Timers();
