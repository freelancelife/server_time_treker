const http = require('http') // Чтобы использовать HTTP-интерфейсы в Node.js
const URL = require('url') // Для разрешения и разбора URL 
const querystring = require('querystring');

const mimeTypes = {
  '.json': 'application/json',
}

const Manager = require('./manager');
const Modules = require('./modules');
const Module = require('./module');
const Respons = require('./respons');
const Registry = require('./registry');
const modules = new Modules();

const auth = require('./lib/auth/index');

//const auth = require('./lib/auth')({
//  userName: 'test',
//  password: 'test',
//  secret: 'test',
//  data: {type: 'user'}
//});

modules.set(new Module('add', './modules/add.js', ['model', 'set']));
modules.set(new Module('update', './modules/update.js', ['model', 'set','where']));
modules.set(new Module('list', './modules/list.js', ['model']));
modules.set(new Module('view', './modules/view.js', ['model', 'id']));
modules.set(new Module('del', './modules/del.js', ['model', 'id']));
modules.set(new Module('excel', './modules/excelCreater.js',['model', 'where']));
modules.set(new Module('register', './modules/register.js',['set'], false));
modules.set(new Module('signin', './modules/signin.js',['where'], false));
modules.set(new Module('descriptor',
                       './modules/descriptorModel.js', ['model']));

const manager  = new Manager(modules, Respons, Registry, auth);

//Инициализируем глобальные переменные
require('./config.init.js')();
//Инициализируем модели
require('./model/init.js')();

const server = http.createServer();

server.on('request', (req, res) => {
  const query = URL.parse(req.url,true).query;

  if(req.headers.hasOwnProperty('origin')) {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  }

  var body = "";
  req.setEncoding('utf8');

  req.on('data', (chunk) => {
    body += chunk;
  });

  req.on('end', () => {
    var reqData = query; 

    if(req.method === 'POST') {
      let postData = querystring.parse(body);

      if(typeof postData === 'object'
      && postData.body) {
        reqData = {
          ...query,
          data: postData.body
        }
      }
    }

    //console.log(JSON.parse(reqData.data));

    manager.handle(reqData, res)
    .then(respons => {
      console.log("\r", new Date().toJSON(),' - ', req.url)
      console.log("\r", 'УСПЕХ: ');

      //Подумать как сделать лучше!!!
      if(respons instanceof Function) {
        return respons(res);
      }

      res.writeHead(200, {'Content-Type': mimeTypes['.json']});
      res.end(respons.json());
    })
    .catch(err => {
      console.log("\r", new Date().toJSON(),' - ', req.url)
      console.log('ERROR: ', err);
      res.writeHead(500, {'Content-Type': mimeTypes['.json']});

      if(err instanceof Respons) {
        return res.end(err.json());
      }

      if(err instanceof Error && err.name === 'JsonWebTokenError') {
        return res.end(new Respons(null, 2).json());
      }

      res.end(new Respons(null, 500, err).json());
    })

  });

});

server.listen(process.env.PORT, () => {
  console.log('Server listening on ' + process.env.HOST +':'+ (process.env.PORT));
});
